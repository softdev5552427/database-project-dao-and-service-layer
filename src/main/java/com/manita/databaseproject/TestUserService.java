/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.manita.databaseproject;

import com.manita.databaseproject.model.User;
import com.manita.databaseproject.service.UserService;

/**
 *
 * @author user
 */
public class TestUserService {
    public static void main(String[] args) {
        UserService userservice = new UserService();
        User user = userservice.login("user2", "password");
        if(user!=null) {
            System.out.println("Welcome user: "+ user.getName());
        }else {
            System.out.println("Error");
        }
                
    }
}
