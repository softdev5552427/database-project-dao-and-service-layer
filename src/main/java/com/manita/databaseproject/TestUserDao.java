/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.manita.databaseproject;

import com.manita.databaseproject.dao.UserDao;
import com.manita.databaseproject.helper.DatabaseHelper;
import com.manita.databaseproject.model.User;

/**
 *
 * @author user
 */
public class TestUserDao {

    public static void main(String[] args) {
        UserDao userdao = new UserDao();
//      System.out.println(userdao.getAll());
//        for (User u : userdao.getAll()) {
//            System.out.println(u); //GetAll
//        }
//        User user1 = userdao.get(2);
//        System.out.println(user1);   //GetByID

//        User newUser = new User("user3", "password", 2, "F");
//        User insetedUser = userdao.save(newUser);
//        System.out.println(insetedUser);   //Save
//        user1.setGender("F");
//        userdao.update(user1);
//        User updateUser = userdao.get(user1.getId());
//        System.out.println(updateUser);     //Update

//        userdao.delete(user1); //Delete
//        for (User u : userdao.getAll()) {
//            System.out.println(u); //GetAll
//        }

        for (User u : userdao.getAllOrderBy("user_name", "asc")) {   //getByOrder
            System.out.println(u); //GetAll
        }
        
        
        DatabaseHelper.close();  //close

    }
}
